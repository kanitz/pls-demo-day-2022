# Demo day

It's time to try out the different tools you created :)

The idea is that we test the tools from a _user_ perspective, i.e., a random
person who might have stumbled upon your tool and would like to test. The
person would be assumed to have some very basic bioinformatics and programming
abilities. Based on the documentation and the success in installing, testing
and running a tool, the basic underlying question for each tool would be:

> "Would I feel comfortable in using that tool for my work?"

We will follow the order in which the tools would be executed in the eventual
workflow according to the following schema:

![schema](schema.png)

So, that is the following order

1. [Transcript sampler](https://git.scicore.unibas.ch/zavolan_group/tools/transcript-sampler)
2. [Transcript structure generator](https://git.scicore.unibas.ch/zavolan_group/tools/transcript-structure-generator)
3. [Transcript sequence extractor](https://git.scicore.unibas.ch/zavolan_group/tools/transcript-sequence-extractor)
4. [Priming site predictor](https://git.scicore.unibas.ch/zavolan_group/tools/priming-site-predictor)
5. [cDNA generator](https://git.scicore.unibas.ch/zavolan_group/tools/cdna-generator)
6. [Terminal fragment selector](https://git.scicore.unibas.ch/zavolan_group/tools/terminal-fragment-selector)
7. [Read sequencer](https://git.scicore.unibas.ch/zavolan_group/tools/read-sequencer)

## Transcript sampler

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/transcript-sampler.git
cd transcript sampler
```

Create Conda environment

```bash
conda create --clone=base --yes --name=transcript-sampler
```

Activate Conda environment

```bash
conda activate transcript-sampler
```

### Install

> No installation instructions, no `requirements.txt` or `environment.yaml`, no packaging!

### Run tests

> No testing instructions, so I will try my luck:

```bash
coverage run --source scripts -m pytest; coverage report -m
```

Output:

```console
bash: /home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/bin/pytest: /home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/bin/python3.6: bad interpreter: No such file or directory
```

Install `pytest` & `coverage`:

```bash
mamba install pytest coverage
```

Rerun test runner as above. Output:

```console
============================= test session starts ==============================
platform linux -- Python 3.10.7, pytest-7.2.0, pluggy-1.0.0
rootdir: /home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-sampler
collected 0 items / 2 errors                                                   

==================================== ERRORS ====================================
______ ERROR collecting test/Test_representative_and_match/test_match.py _______
ImportError while importing test module '/home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-sampler/test/Test_representative_and_match/test_match.py'.
Hint: make sure your test modules/packages have valid Python names.
Traceback:
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/importlib/__init__.py:126: in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
test/Test_representative_and_match/test_match.py:4: in <module>
    import match_reprtranscript_expressionlevel as match
E   ModuleNotFoundError: No module named 'match_reprtranscript_expressionlevel'
__ ERROR collecting test/Test_representative_and_match/test_representative.py __
ImportError while importing test module '/home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-sampler/test/Test_representative_and_match/test_representative.py'.
Hint: make sure your test modules/packages have valid Python names.
Traceback:
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/importlib/__init__.py:126: in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
test/Test_representative_and_match/test_representative.py:3: in <module>
    import datatest as dt
E   ModuleNotFoundError: No module named 'datatest'
=========================== short test summary info ============================
ERROR test/Test_representative_and_match/test_match.py
ERROR test/Test_representative_and_match/test_representative.py
!!!!!!!!!!!!!!!!!!! Interrupted: 2 errors during collection !!!!!!!!!!!!!!!!!!!!
============================== 2 errors in 0.74s ===============================
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/coverage/control.py:825: CoverageWarning: No data was collected. (no-data-collected)
  self._warn("No data was collected.", slug="no-data-collected")
```

Perhaps the call was a little too ambitious. Let's try with a simpler one:

```bash
pytest
```

Output:

```console
============================================== test session starts ===============================================
platform linux -- Python 3.10.7, pytest-7.2.0, pluggy-1.0.0
rootdir: /home/user/repos/transcript-sampler
collected 0 items / 2 errors                                                                                     

===================================================== ERRORS =====================================================
_______________________ ERROR collecting test/Test_representative_and_match/test_match.py ________________________
ImportError while importing test module '/home/user/repos/transcript-sampler/test/Test_representative_and_match/test_match.py'.
Hint: make sure your test modules/packages have valid Python names.
Traceback:
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/importlib/__init__.py:126: in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
test/Test_representative_and_match/test_match.py:4: in <module>
    import match_reprtranscript_expressionlevel as match
E   ModuleNotFoundError: No module named 'match_reprtranscript_expressionlevel'
___________________ ERROR collecting test/Test_representative_and_match/test_representative.py ___________________
ImportError while importing test module '/home/user/repos/transcript-sampler/test/Test_representative_and_match/test_representative.py'.
Hint: make sure your test modules/packages have valid Python names.
Traceback:
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/importlib/__init__.py:126: in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
test/Test_representative_and_match/test_representative.py:3: in <module>
    import datatest as dt
E   ModuleNotFoundError: No module named 'datatest'
============================================ short test summary info =============================================
ERROR test/Test_representative_and_match/test_match.py
ERROR test/Test_representative_and_match/test_representative.py
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Interrupted: 2 errors during collection !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
=============================================== 2 errors in 0.60s ================================================
```

> Giving up on running tests!

### Run tool

Following instructions on <https://git.scicore.unibas.ch/zavolan_group/tools/transcript-sampler>

Trying example call:

```bash
python scripts/new_exe.py --annotation "input_files/test.gtf" --output_csv "output_files/output_csv.txt" --transcript_number 50  --output_gtf "output_files/output_gtf.gtf" --input_csv "input_files/expression.csv"
```

Output:

```console
Traceback (most recent call last):
  File "/home/user/repos/transcript-sampler/scripts/new_exe.py", line 6, in <module>
    import representative as rtcl
  File "/home/user/repos/transcript-sampler/scripts/representative.py", line 1, in <module>
    import pandas as pd
ModuleNotFoundError: No module named 'pandas'
```

```bash
mamba install pandas
```

Repeat example call. Output:

```console
No .gtf file of the name input_files/test.gtf has been found in this pathway
[##############################]	100%		0
The transcripts have been collected
Transcripts are filtered based on transcript score. Please wait...
Transcripts filtered

Representative trascipts are filterd based on exon length please wait...
[##############################]	100%		0

Representative transcripts collected
Finiding match between representative transcripts and expression level file
Poisson sampling of transcripts
Traceback (most recent call last):
  File "/home/user/repos/transcript-sampler/scripts/new_exe.py", line 41, in <module>
    exe(args.annotation, args.output_csv, args.output_gtf, args.input_csv, args.transcript_number)
  File "/home/user/repos/transcript-sampler/scripts/new_exe.py", line 25, in exe
    ps.transcript_sampling(transcript_nr, df_repr, csv)
  File "/home/user/repos/transcript-sampler/scripts/poisson_sampling.py", line 39, in transcript_sampling
    pd.DataFrame.to_csv(transcript_numbers, output_csv)
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/util/_decorators.py", line 211, in wrapper
    return func(*args, **kwargs)
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/core/generic.py", line 3720, in to_csv
    return DataFrameRenderer(formatter).to_csv(
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/util/_decorators.py", line 211, in wrapper
    return func(*args, **kwargs)
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/io/formats/format.py", line 1189, in to_csv
    csv_formatter.save()
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/io/formats/csvs.py", line 241, in save
    with get_handle(
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/io/common.py", line 734, in get_handle
    check_parent_directory(str(handle))
  File "/home/uniqueg/Software/miniconda3/envs/pls-transcript-sampler/lib/python3.10/site-packages/pandas/io/common.py", line 597, in check_parent_directory
    raise OSError(rf"Cannot save file into a non-existent directory: '{parent}'")
OSError: Cannot save file into a non-existent directory: 'output_files'
```

Create output directory:

```bash
mkdir output_files
```

Rerun example call. Output:

```console
[##############################]	100%		0
The transcripts have been collected
Transcripts are filtered based on transcript score. Please wait...
Transcripts filtered

Representative trascipts are filterd based on exon length please wait...
[##############################]	100%		0

Representative transcripts collected
Finiding match between representative transcripts and expression level file
Poisson sampling of transcripts
output csv file ready
writing output gtf file
```

### Check Git status

```bash
git status
```

Output:

```console
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	.coverage
	No
	input_files/test_intermediate_file.txt
	output_files/
	scripts/__pycache__/
	test/Test_representative_and_match/__pycache__/

nothing added to commit but untracked files present (use "git add" to track)
```

> A lot of files are created that Git does not ignore!

### Check output

> Output description in docs is minimal!

```bash
head output_files/output_csv.txt
```

Output:

```console
,id,count
0,ENST00000472194,19
1,ENST00000442483,15
7,ENST00000378391,11

```

> Looks okay, but what's up with the first column? As we will see later, the
> next tool down the line does not expect _three_ columns!

```bash
head output_files/output_gtf.txt
```

Output:

```console
1	havana	transcript	1478026	1497848	.	+	.	gene_id "ENSG00000160072"; gene_version "20"; transcript_id "ENST00000472194"; transcript_version "6"; gene_name "ATAD3B"; gene_source "ensembl_havana"; gene_biotype "protein_coding"; transcript_name "ATAD3B-203"; transcript_source "havana"; transcript_biotype "retained_intron"; transcript_support_level "1";
1	havana	transcript	2212523	2220738	.	+	.	gene_id "ENSG00000234396"; gene_version "3"; transcript_id "ENST00000442483"; transcript_version "2"; gene_source "havana"; gene_biotype "lncRNA"; transcript_source "havana"; transcript_biotype "lncRNA"; tag "basic"; transcript_support_level "3";
1	havana	transcript	3069197	3435421	.	+	.	gene_id "ENSG00000142611"; gene_version "17"; transcript_id "ENST00000378391"; transcript_version "6"; gene_name "PRDM16"; gene_source "ensembl_havana"; gene_biotype "protein_coding"; transcript_name "PRDM16-203"; transcript_source "havana"; transcript_biotype "protein_coding"; tag "CCDS"; ccds_id "CCDS44048"; tag "basic"; transcript_support_level "1";
```

> Looks like only expressed transcripts are included, nice!

### Docker / Nextflow

> There doesn't seem to be a `Dockerfile` or Nextflow process available!

### Conclusion

Unfortunately, the lack of packaging, dependencies and `Dockerfile` makes it
difficult to install and run the tool, or include it in a workflow. There is
also no Nextflow process available. The tests didn't run either. After
installing dependencies and creating an output directory, the example call
completed successfully. The outputs are not fully described, but otherwise
look plausible. However, not much confidence after all the issues/hiccups, so I
would probably _not_ use the tool.

## Transcript structure generator

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/transcript-structure-generator.git
cd transcript-structure-generator
```

Create Conda environment

```bash
conda create --clone=base --yes --name transcript-structure-generator
```

Activate Conda environment

```bash
conda activate transcript-structure-generator
```

### Install

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/transcript-structure-generator

> Instructions for setting up a Conda environment are available. As we already
> have a Conda environment, we will update it instead. We need to explicitly
> provide the name of our environment so that we don't use the name that is
> specified in `environment.yml`!

```bash
mamba env update --file environment.yml --name transcript-structure-generator
```

> The environment is already activated!

Let's install the package and dependencies:

```bash
pip install "setuptools>=62.1.0"
pip install .
```

> Installation seems to have worked nicely! For the `setuptools` installation
> instructions, I saw that there is a small problem though. Bash (and other
> POSIX shells) interpret the `>` character as an output redirector. So the
> whole expression `setuptools>=62.1.0` should go in double quotes as above.

### Run tests

> Instructions for running tests are provided. Let's try them!

```bash
pytest tests
```

Output:

```console
============================================== test session starts ===============================================
platform linux -- Python 3.9.15, pytest-7.1.2, pluggy-1.0.0
rootdir: /home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-structure-generator
collected 10 items                                                                                               

tests/test_main.py .....FF...                                                                              [100%]

==================================================== FAILURES ====================================================
______________________________________ TestTranscriptGenerator.test_init_2 _______________________________________

self = <tests.test_main.TestTranscriptGenerator object at 0x7f142e1f6df0>

    def test_init_2(self):
        with pytest.raises(AssertionError):
>           transcripts = TranscriptGenerator("TRANSCRIPT2", 3, self.df2, 0.05)
E           Failed: DID NOT RAISE <class 'AssertionError'>

tests/test_main.py:107: Failed
----------------------------------------------- Captured log call ------------------------------------------------
WARNING  tsg.main:main.py:291 Transcript TRANSCRIPT2 can't be sampled. Annotation is missing
______________________________________ TestTranscriptGenerator.test_init_3 _______________________________________

self = <tests.test_main.TestTranscriptGenerator object at 0x7f142e1f6f10>

    def test_init_3(self):
        with pytest.raises(AssertionError):
>           transcripts = TranscriptGenerator("TRANSCRIPT1", 0, self.df1, 0.05)
E           Failed: DID NOT RAISE <class 'AssertionError'>

tests/test_main.py:111: Failed
============================================ short test summary info =============================================
FAILED tests/test_main.py::TestTranscriptGenerator::test_init_2 - Failed: DID NOT RAISE <class 'AssertionError'>
FAILED tests/test_main.py::TestTranscriptGenerator::test_init_3 - Failed: DID NOT RAISE <class 'AssertionError'>
========================================== 2 failed, 8 passed in 0.36s ===========================================
```

> So two tests failed. Hmm, not really sure what that means. Let's check what
> the test coverage is.

```bash
coverage run --source tsg -m pytest; coverage report -m
```

Output:

```console
============================================== test session starts ===============================================
platform linux -- Python 3.9.15, pytest-7.1.2, pluggy-1.0.0
rootdir: /home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-structure-generator
collected 10 items                                                                                               

tests/test_main.py .....FF...                                                                              [100%]

==================================================== FAILURES ====================================================
______________________________________ TestTranscriptGenerator.test_init_2 _______________________________________

self = <tests.test_main.TestTranscriptGenerator object at 0x7f83da7ab5b0>

    def test_init_2(self):
        with pytest.raises(AssertionError):
>           transcripts = TranscriptGenerator("TRANSCRIPT2", 3, self.df2, 0.05)
E           Failed: DID NOT RAISE <class 'AssertionError'>

tests/test_main.py:107: Failed
----------------------------------------------- Captured log call ------------------------------------------------
WARNING  tsg.main:main.py:291 Transcript TRANSCRIPT2 can't be sampled. Annotation is missing
______________________________________ TestTranscriptGenerator.test_init_3 _______________________________________

self = <tests.test_main.TestTranscriptGenerator object at 0x7f83da7ab6d0>

    def test_init_3(self):
        with pytest.raises(AssertionError):
>           transcripts = TranscriptGenerator("TRANSCRIPT1", 0, self.df1, 0.05)
E           Failed: DID NOT RAISE <class 'AssertionError'>

tests/test_main.py:111: Failed
============================================ short test summary info =============================================
FAILED tests/test_main.py::TestTranscriptGenerator::test_init_2 - Failed: DID NOT RAISE <class 'AssertionError'>
FAILED tests/test_main.py::TestTranscriptGenerator::test_init_3 - Failed: DID NOT RAISE <class 'AssertionError'>
========================================== 2 failed, 8 passed in 0.79s ===========================================
Name              Stmts   Miss  Cover   Missing
-----------------------------------------------
tsg/__init__.py       4      0   100%
tsg/__main__.py       3      3     0%   2-5
tsg/cli.py           37     37     0%   2-130
tsg/main.py         151     66    56%   25-30, 57, 123-128, 141-143, 160-161, 206, 230-235, 261-271, 275, 296-301, 333-334, 357, 382-383, 405-408, 419-428, 447-480
-----------------------------------------------
TOTAL               195    106    46%
```

> Looks like almost half of the code in the single main code module is not
> executed during test runs!

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

> Inputs/params are nicely described and the general usage is available.
> Unfortunately there is no example call. But found something that looks like
> a set of inputs in `tests/resources`. Let's inspect them:

```bash
head tests/resources/Transcript1.csv
```

Output:

```console
TRANSCRIPT1	92
TRANSCRIPT2	13
TRANSCRIPT3	73
TRANSCRIPT4	83
TRANSCRIPT5	32
TRANSCRIPT6	136
TRANSCRIPT7	36
```

```bash
head tests/resources/Annotation1.gtf
```

Output:

```console
1	havana	gene	1	100	.	+	.	gene_id "GENE1";
1	havana	transcript	1	100	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1"; transcript_support_level "1";
1	havana	exon	1	20	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1"; exon_number "1"; exon_id "ENSE00002234944"; transcript_support_level "1";
1	havana	exon	50	70	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1"; exon_number "2"; exon_id "ENSE00003582793"; transcript_support_level "1";
1	havana	exon	80	100	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1"; exon_number "3"; exon_id "ENSE00002312635"; transcript_support_level "1";
```

> Files look fine, so let's try to use them based on the usage instructions:

```bash
transcript-generator \
    --prob_inclusion 0.5 \
    --log "DEBUG" \
    tests/resources/Transcript1.csv \
    tests/resources/Annotation1.gtf
```

Output:

```console
usage: transcript-generator [-h] [-p PROB_INCLUSION] [--log LOG] transcripts annotation
transcript-generator: error: unrecognized arguments: --prob_inclusion tests/resources/Annotation1.gtf
```

> Seems there is a little typo in the option name. Probably it's
> `--prob-inclusion` instead? Let's try:

```bash
transcript-generator \
    --prob-inclusion 0.5 \
    --log "DEBUG" \
    tests/resources/Transcript1.csv \
    tests/resources/Annotation1.gtf
```

Output:

```console
[2022-12-21 09:16:42,527: INFO] Probability of intron inclusion: 0.5 (module "main")
[2022-12-21 09:16:42,527: INFO] Parsing transcript abundances... (module "main")
[2022-12-21 09:16:42,530: INFO] Done parsing... (module "main")
[2022-12-21 09:16:42,530: INFO] Parsing annotations... (module "main")
[2022-12-21 09:16:42,535: INFO] Done parsing... (module "main")
[2022-12-21 09:16:42,535: INFO] Start sampling transcripts... (module "main")
0it [00:00, ?it/s][2022-12-21 09:16:42,558: DEBUG] Transcript TRANSCRIPT1 sampled (module "main")
[2022-12-21 09:16:42,562: WARNING] Transcript TRANSCRIPT2 can't be sampled. Annotation is missing (module "main")
[2022-12-21 09:16:42,564: WARNING] Transcript TRANSCRIPT3 can't be sampled. Annotation is missing (module "main")
[2022-12-21 09:16:42,567: WARNING] Transcript TRANSCRIPT4 can't be sampled. Annotation is missing (module "main")
[2022-12-21 09:16:42,569: WARNING] Transcript TRANSCRIPT5 can't be sampled. Annotation is missing (module "main")
[2022-12-21 09:16:42,570: WARNING] Transcript TRANSCRIPT6 can't be sampled. Annotation is missing (module "main")
[2022-12-21 09:16:42,572: WARNING] Transcript TRANSCRIPT7 can't be sampled. Annotation is missing (module "main")
7it [00:00, 219.55it/s]
[2022-12-21 09:16:42,572: INFO] Done. (module "main")
```

> Finished without errors - nice! Most of the transcript annotations are
> missing in the example input files, so we get quite some warnings, but guess
> that's okay.

### Check Git status

```bash
git status
```

Output:

```console
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   environment.yml

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	=62.1.0
	generated_Annotation1.gtf
	generated_Transcript1.csv

no changes added to commit (use "git add" and/or "git commit -a")
```

> Seems the outputs have just been written to some files whose file names I
> did not have any influence over. That can be a problem, but okay. Of course,
> these files should be excluded from version control, so they should be added
> to `.gitignore`. I've also found out that rerunning that same call from above
> will fail a second time around, because the output files are already there.
> That's good for safety perhaps, but bad for user experience. Best is to
> give the user control over where they want to store outputs.

### Check output

> Outputs are fairly well described.

```bash
head generated_Transcript1.csv 
```

Output:

```console
TRANSCRIPT1_0,TRANSCRIPT1,26
TRANSCRIPT1_1,TRANSCRIPT1,24
TRANSCRIPT1_2,TRANSCRIPT1,25
TRANSCRIPT1_3,TRANSCRIPT1,17
```

```bash
head generated_Annotation1.gtf 
```

Output:

```console
seqname	source	feature	start	end	score	strand	frame	free_text
1	havana	exon	1	20	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_0"; exon_number "1"; exon_id "ENSE00002234944"; transcript_support_level "1";
1	havana	exon	50	70	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_0"; exon_number "2"; exon_id "ENSE00003582793"; transcript_support_level "1";
1	havana	exon	80	100	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_0"; exon_number "3"; exon_id "ENSE00002312635"; transcript_support_level "1";
1	havana	exon	1	20	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_1"; exon_number "1"; exon_id "ENSE00002234944"; transcript_support_level "1";
1	havana	exon	50	79	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_1"; exon_number "2"; exon_id "ENSE00003582793_1"; transcript_support_level "1";
1	havana	exon	80	100	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_1"; exon_number "3"; exon_id "ENSE00002312635"; transcript_support_level "1";
1	havana	exon	1	49	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_2"; exon_number "1"; exon_id "ENSE00002234944_0"; transcript_support_level "1";
1	havana	exon	50	70	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_2"; exon_number "2"; exon_id "ENSE00003582793"; transcript_support_level "1";
1	havana	exon	80	100	.	+	.	gene_id "GENE1"; transcript_id "TRANSCRIPT1_2"; exon_number "3"; exon_id "ENSE00002312635"; transcript_support_level "1";
```

> At first glance, at least structurally, these outputs look okay. Would need
> to dig deeper into what the tool does, but it's rather comforting that I only
> entries for the one transcript that could be sampled in the example run.

### Docker / Nextflow

> A `Dockerfile` is available. Let's see if it builds with the call indicated
> in the instructions.

```bash
docker build -t transcript-app .
```

Output:

```console
Sending build context to Docker daemon   2.57MB
Step 1/7 : FROM python:3.9-slim-buster
 ---> 83e74122cf8a
Step 2/7 : WORKDIR /usr/src/app
 ---> Running in 3fd764a1c3f0
Removing intermediate container 3fd764a1c3f0
 ---> ebbbfac8c654
Step 3/7 : COPY README.md pyproject.toml ./
 ---> fbc89bc63415
Step 4/7 : ADD tsg ./tsg
 ---> 5818439388f1
Step 5/7 : ADD data ./data
ADD failed: stat /var/lib/docker/tmp/docker-builder327241844/data: no such file or directory
```

> There is no file or folder `data`, so the `ADD` directive fails. Probably
> easy to fix, but not going there now. A Nextflow process is not available.

### Conclusion

> The instructions were pretty good, so it was rather easy to install, test and
> run the tool. An example call would have been nice, ideally with larger test
> inputs so that we would get some more output. Also the failing tests
> and low coverage are not very comforting. I might still use the tool, or at
> least I would probably bother raising issues for the problems I found and
> would feel much more comfortable if I get responses or fixes for these.

## Transcript sequence extractor

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/transcript-sequence-extractor.git
cd transcript-sequence-extractor
```

Create Conda environment

```bash
conda create --clone=base --yes --name transcript-sequence-extractor
```

Activate Conda environment

```bash
conda activate transcript-sequence-extractor
```

### Install

> No installation instructions available, but found _both_ an `environment.yml`
> and a pair of `requirements.txt` and `requirements_dev.txt` files. Going with
> Conda. As above I'm updating the existing environment:

```bash
mamba env update --file environment.yml --name transcript-sequence-extractor
```

> Setting up the environment seems to have worked out nicely. The package
> itself is not included, so let's try to do that manually with `pip`.

```bash
pip install -e .
```

> Also worked out well! Would be easy to add those two commands to the
> `README.md`. I would remove the `requirements.txt` and `requirements_dev`
> files so that there is a single source for the dependencies. Also wondering
> what's up with the `gtf_processing` directory. It's not a package and it's
> not included in the packaging, so if it contains code that is needed, it
> might not end up on the users' machines/environments or at least limit
> portability for use in workflows etc. So if the code is needed, add it to
> the package as a separate subpackage or submodule. Otherwise, delete it.

### Run tests

> No instructions available for running tests, so let's try our luck.

```bash
coverage run --source sequence_extractor -m pytest; coverage report -m
```

Output:

```console
============================================== test session starts ===============================================
platform linux -- Python 3.10.8, pytest-7.2.0, pluggy-1.0.0
rootdir: /home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-sequence-extractor
collected 0 items / 2 errors                                                                                     

===================================================== ERRORS =====================================================
_______________________________ ERROR collecting tests/test_exon_concatenation.py ________________________________
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/site-packages/_pytest/python.py:618: in _importtestmodule
    mod = import_path(self.path, mode=importmode, root=self.config.rootpath)
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/site-packages/_pytest/pathlib.py:533: in import_path
    importlib.import_module(module_name)
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/importlib/__init__.py:126: in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
<frozen importlib._bootstrap>:1050: in _gcd_import
    ???
<frozen importlib._bootstrap>:1027: in _find_and_load
    ???
<frozen importlib._bootstrap>:1006: in _find_and_load_unlocked
    ???
<frozen importlib._bootstrap>:688: in _load_unlocked
    ???
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/site-packages/_pytest/assertion/rewrite.py:159: in exec_module
    source_stat, co = _rewrite_test(fn, self.config)
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/site-packages/_pytest/assertion/rewrite.py:337: in _rewrite_test
    tree = ast.parse(source, filename=strfn)
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/ast.py:50: in parse
    return compile(source, filename, mode, flags,
E     File "/home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-sequence-extractor/tests/test_exon_concatenation.py", line 2
E       import exon_concatenation from exon_concatenation
E                                 ^^^^
E   SyntaxError: invalid syntax
_____________________________________ ERROR collecting tests/test_poly_a.py ______________________________________
ImportError while importing test module '/home/uniqueg/Dropbox/repos/__teaching__/pls_repos/transcript-sequence-extractor/tests/test_poly_a.py'.
Hint: make sure your test modules/packages have valid Python names.
Traceback:
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/importlib/__init__.py:126: in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
tests/test_poly_a.py:2: in <module>
    from poly_a import poly_a_generator, poly_a_addition_to_fasta_list
E   ModuleNotFoundError: No module named 'poly_a'
============================================ short test summary info =============================================
ERROR tests/test_exon_concatenation.py
ERROR tests/test_poly_a.py
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Interrupted: 2 errors during collection !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
=============================================== 2 errors in 0.25s ================================================
/home/uniqueg/Software/miniconda3/envs/pls-transcript-sequence-extractor/lib/python3.10/site-packages/coverage/control.py:801: CoverageWarning: No data was collected. (no-data-collected)
  self._warn("No data was collected.", slug="no-data-collected")
Name                                       Stmts   Miss  Cover   Missing
------------------------------------------------------------------------
sequence_extractor/__init__.py                 0      0   100%
sequence_extractor/cli.py                     19     19     0%   2-40
sequence_extractor/exon_concatenation.py      16     16     0%   4-30
sequence_extractor/poly_a.py                   8      8     0%   4-39
------------------------------------------------------------------------
TOTAL                                         43     43     0%
```

> Ugh, that didn't work out too well. The errors prevent `pytest` from even
> collecting the test cases, and so we don't know if they actually succeeded.
> Something seems to be wrong with importing modules in both cases. As an
> average user, I probably wouldn't know how to fix this easily, so let's give
> up on testing here.

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/transcript-sequence-extractor

> There is some description of inputs and outputs, but it's not very detailed,
> and there does not seem to be an example call, just a basic usage (apparently
> not listing all parameters). There seem to be some test files available in
> `tests/test_files`. Let's have a look.

```bash
head tests/test_files/test_1.fa
```

Output:

```console
>ENST00000673477::1:1471765-1472089
TTTCGCCTGCGCAGTGGTCCTGGCCACCGGCTCGCGGCGCGTGGAGGCTGCTCCCAGCCGCGCCCGAGTCAGACTCGGGTGGGGGTCCCGGCGGCGGTAGCGGCGGCGGCGGTGCGAGCATGTCGTGGCTCTTCGGCGTTAACAAGGGCCCCAAGGGTGAAGGCGCGGGGCCGCCGCCGCCTTTGCCGCCCGCGCAGCCCGGGGCCGAGGGCGGCGGGGACCGCGGTTTGGGAGACCGGCCGGCGCCCAAGGACAAATGGAGCAACTTCGACCCCACCGGCCTGGAGCGCGCCGCCAAGGCGGCGCGCGAGCTGGAGCACTCGC
>ENST00000673477::1:1477274-1477350
TTACGCCAAGGAGGCCCTGAATCTGGCGCAGATGCAGGAGCAGACGCTGCAGTTGGAGCAACAGTCCAAGCTCAAA
>ENST00000378391::1:3244087-3244137
AAATACTGACGGACGTGGAAGTGTCGCCCCAGGAAGGCTGCATCACAAAG
>ENST00000378391::1:3385152-3385286
TCTCCGAAGACCTGGGCAGTGAGAAGTTCTGCGTGGATGCAAATCAGGCGGGGGCTGGCAGCTGGCTCAAGTACATCCGTGTGGCGTGCTCCTGCGATGACCAGAACCTCACCATGTGTCAGATCAGTGAGCAG
```

```bash
head tests/test_files/test_2.fa
```

Output:

```console
>ENST00000673477::1:1482545-1482614
ACGGCTGGCACCTTGTTTGGGGAAGGATTCCGTGCCTTTGTGACAGACCGGGACAAAGTGACAGCCACG
>ENST00000673477::1:1485016-1485171
TGGCTGGGCTGACGCTGCTGGCTGTCGGGGTCTACTCAGCCAAGAATGCGACAGCCGTCACTGGCCGCTTCATCGAGGCTCGGCTGGGGAAGCCGTCCCTAGTGAGGGAGACGTCCCGCATCACGGTGCTGGAGGCGCTGCGGCACCCCATCCAG
>ENST00000673477::1:1485782-1485838
TCAGCCGGCGGCTCCTCAGTCGACCCCAGGACGTGCTGGAGGGTGTTGTGCTTAGT
>ENST00000673477::1:1486110-1486235
```

```bash
head tests/test_files/post_bedtools_test.fa
```

Output:

```console
>ENST00000673477::1:1471765-1472089
TTTCGCCTGCGCAGTGGTCCTGGCCACCGGCTCGCGGCGCGTGGAGGCTGCTCCCAGCCGCGCCCGAGTCAGACTCGGGTGGGGGTCCCGGCGGCGGTAGCGGCGGCGGCGGTGCGAGCATGTCGTGGCTCTTCGGCGTTAACAAGGGCCCCAAGGGTGAAGGCGCGGGGCCGCCGCCGCCTTTGCCGCCCGCGCAGCCCGGGGCCGAGGGCGGCGGGGACCGCGGTTTGGGAGACCGGCCGGCGCCCAAGGACAAATGGAGCAACTTCGACCCCACCGGCCTGGAGCGCGCCGCCAAGGCGGCGCGCGAGCTGGAGCACTCGC
>ENST00000673477::1:1477274-1477350
TTACGCCAAGGAGGCCCTGAATCTGGCGCAGATGCAGGAGCAGACGCTGCAGTTGGAGCAACAGTCCAAGCTCAAA
>ENST00000673477::1:1478644-1478745
AGTATGAGGCCGCCGTGGAGCAGCTCAAGAGCGAGCAGATCCGGGCGCAGGCTGAGGAGAGGAGGAAGACCCTGAGCGAGGAGACCCGGCAGCACCAGGCC
>ENST00000673477::1:1479049-1479108
GGGCCCAGTATCAAGACAAGCTGGCCCGGCAGCGCTACGAGGACCAACTGAAGCAGCAG
>ENST00000673477::1:1480867-1480936
AACTTCTCAATGAGGAGAATTTACGGAAGCAGGAGGAGTCCGTGCAGAAGCAGGAAGCCATGCGGCGAG
```

```bash
head tests/test_files/test.bed
```

Output:

```console
1	1471765	1472089	ENST00000673477	.	+	ENSG00000160072
1	1477274	1477350	ENST00000673477	.	+	ENSG00000160072
1	1478644	1478745	ENST00000673477	.	+	ENSG00000160072
1	1479049	1479108	ENST00000673477	.	+	ENSG00000160072
1	1480867	1480936	ENST00000673477	.	+	ENSG00000160072
1	1482138	1482303	ENST00000673477	.	+	ENSG00000160072
1	1482545	1482614	ENST00000673477	.	+	ENSG00000160072
1	1485016	1485171	ENST00000673477	.	+	ENSG00000160072
1	1485782	1485838	ENST00000673477	.	+	ENSG00000160072
1	1486110	1486235	ENST00000673477	.	+	ENSG00000160072
```

> Now, I'm a bit confused as the usage instructions ask for a GTF file - which
> is also what the previous script returns. But what we have here is a _BED_
> file. Perhaps this explains what `gtf_processing/pre_bedtools.py` does? So
> assuming we would _first_ need to run the GTF processing on the GTF input to
> get a BED file, then perhaps we should use the BED rather than GTF in The
> actual `transcript_sequence_extractor` command? Let's give it a try.

```bash
transcript_sequence_extractor \
  --input_fasta_file tests/test_files/post_bedtools_test.fa \
  --input_gtf tests/test_files/test.bed \
  --output_file_name output_unknown_format
```

Output:

```console
transcript_sequence_extractor: command not found
```

> Okay, so it seems that the script is not called
> `transcript_sequence_extractor`. Checking `setup.py` reveals that it should
> be `sequenced_extractor`. Let's try.

```bash
sequence_extractor --help
```

Output:

```console
cd -- sequence_extractor --help
bash: cd: too many arguments
```

> Not sure what happened here! But I guess at this point I would be giving up.

### Check Git status

```bash
git status
```

Output:

```console
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```

> Git seems clean, but then again we didn't actually manage to run anything.

### Check output

> No output was generated.

### Docker / Nextflow

> A `Dockerfile` is available. There are no instructions for building it in
> the doc, but it should be easy enough.

```bash
docker build -t transcript-sequence-extractor .
```

Output:

```console
Sending build context to Docker daemon  2.317MB
Step 1/5 : FROM python:3.10-slim-buster
 ---> f07edae6c645
Step 2/5 : MAINTAINER Samuel Mondal
 ---> Running in 6ac760eb678a
Removing intermediate container 6ac760eb678a
 ---> 14bdb02ccc4a
Step 3/5 : ENV PATH=$PATH:/usr/local/
 ---> Running in 92e38059df86
Removing intermediate container 92e38059df86
 ---> 00c2798ebe14
Step 4/5 : COPY . .
 ---> 48bbfba101f0
Step 5/5 : RUN pip install -e .
 ---> Running in 3d94328c7f9a
Obtaining file:///
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting pandas~=1.5
  Downloading pandas-1.5.2-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (12.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 12.1/12.1 MB 22.0 MB/s eta 0:00:00
Collecting numpy~=1.23
  Downloading numpy-1.24.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (17.3 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 17.3/17.3 MB 27.1 MB/s eta 0:00:00
Collecting gtfparse~=1.2
  Downloading gtfparse-1.3.0.tar.gz (16 kB)
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting pytz>=2020.1
  Downloading pytz-2022.7-py2.py3-none-any.whl (499 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 499.4/499.4 kB 18.9 MB/s eta 0:00:00
Collecting python-dateutil>=2.8.1
  Downloading python_dateutil-2.8.2-py2.py3-none-any.whl (247 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 247.7/247.7 kB 15.7 MB/s eta 0:00:00
Collecting six>=1.5
  Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
Building wheels for collected packages: gtfparse
  Building wheel for gtfparse (setup.py): started
  Building wheel for gtfparse (setup.py): finished with status 'done'
  Created wheel for gtfparse: filename=gtfparse-1.3.0-py3-none-any.whl size=15736 sha256=0c99cd20184ef0c0d0ae8cb9b02762905b8afbc5a4829ff96e62a68e5ef4c8c8
  Stored in directory: /root/.cache/pip/wheels/16/61/14/79a5d19ca93f1e929cd622fb835e2c5ccca1325c382be6d550
Successfully built gtfparse
Installing collected packages: pytz, six, numpy, python-dateutil, pandas, gtfparse, sequence-extractor
  Running setup.py develop for sequence-extractor
Successfully installed gtfparse-1.3.0 numpy-1.24.0 pandas-1.5.2 python-dateutil-2.8.2 pytz-2022.7 sequence-extractor-0.0.1 six-1.16.0
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

[notice] A new release of pip available: 22.2.1 -> 22.3.1
[notice] To update, run: pip install --upgrade pip
Removing intermediate container 3d94328c7f9a
 ---> 30e71b76319b
Successfully built 30e71b76319b
Successfully tagged transcript-sequence-extractor:latest
```

> The Docker image builds nicely! I haven't found a Nextflow process.

### Conclusion

> Given the lack of documentation and the fact that I was unable to run tests
> _or_ the tool, I wouldn't be able to actually use it.

## TODO

## {ADD}

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/{ADD}.git
cd {ADD}
```

Create Conda environment

```bash
conda create --clone=base --yes --name {ADD}
```

Activate Conda environment

```bash
conda activate {ADD}
```

### Install

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

> {ADD}!

### Run tests

```bash
coverage run --source {ADD} -m pytest; coverage report -m
```

Output:

```console
{ADD}
```

> {ADD}!

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

Trying example call:

```bash
```

Output:

```console
{ADD}
```

### Check Git status

```bash
git status
```

Output:

```console
{ADD}
```

> {ADD}

### Check output

> {ADD}

```bash
head {ADD}
```

Output:

```console
{ADD}
```

> {ADD}

### Docker / Nextflow

> {ADD}

### Conclusion

{ADD}

## {ADD}

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/{ADD}.git
cd {ADD}
```

Create Conda environment

```bash
conda create --clone=base --yes --name {ADD}
```

Activate Conda environment

```bash
conda activate {ADD}
```

### Install

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

> {ADD}!

### Run tests

```bash
coverage run --source {ADD} -m pytest; coverage report -m
```

Output:

```console
{ADD}
```

> {ADD}!

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

Trying example call:

```bash
```

Output:

```console
{ADD}
```

### Check Git status

```bash
git status
```

Output:

```console
{ADD}
```

> {ADD}

### Check output

> {ADD}

```bash
head {ADD}
```

Output:

```console
{ADD}
```

> {ADD}

### Docker / Nextflow

> {ADD}

### Conclusion

{ADD}

## {ADD}

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/{ADD}.git
cd {ADD}
```

Create Conda environment

```bash
conda create --clone=base --yes --name {ADD}
```

Activate Conda environment

```bash
conda activate {ADD}
```

### Install

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

> {ADD}!

### Run tests

```bash
coverage run --source {ADD} -m pytest; coverage report -m
```

Output:

```console
{ADD}
```

> {ADD}!

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

Trying example call:

```bash
```

Output:

```console
{ADD}
```

### Check Git status

```bash
git status
```

Output:

```console
{ADD}
```

> {ADD}

### Check output

> {ADD}

```bash
head {ADD}
```

Output:

```console
{ADD}
```

> {ADD}

### Docker / Nextflow

> {ADD}

### Conclusion

{ADD}

## {ADD}

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/{ADD}.git
cd {ADD}
```

Create Conda environment

```bash
conda create --clone=base --yes --name {ADD}
```

Activate Conda environment

```bash
conda activate {ADD}
```

### Install

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

> {ADD}!

### Run tests

```bash
coverage run --source {ADD} -m pytest; coverage report -m
```

Output:

```console
{ADD}
```

> {ADD}!

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

Trying example call:

```bash
```

Output:

```console
{ADD}
```

### Check Git status

```bash
git status
```

Output:

```console
{ADD}
```

> {ADD}

### Check output

> {ADD}

```bash
head {ADD}
```

Output:

```console
{ADD}
```

> {ADD}

### Docker / Nextflow

> {ADD}

### Conclusion

{ADD}















## {ADD}

### Clone repository

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/zavolan_group/tools/{ADD}.git
cd {ADD}
```

Create Conda environment

```bash
conda create --clone=base --yes --name {ADD}
```

Activate Conda environment

```bash
conda activate {ADD}
```

### Install

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

> {ADD}!

### Run tests

```bash
coverage run --source {ADD} -m pytest; coverage report -m
```

Output:

```console
{ADD}
```

> {ADD}!

### Run tool

Following instructions on https://git.scicore.unibas.ch/zavolan_group/tools/{ADD}

Trying example call:

```bash
```

Output:

```console
{ADD}
```

### Check Git status

```bash
git status
```

Output:

```console
{ADD}
```

> {ADD}

### Check output

> {ADD}

```bash
head {ADD}
```

Output:

```console
{ADD}
```

> {ADD}

### Docker / Nextflow

> {ADD}

### Conclusion

{ADD}




    






python scripts/new_exe.py --annotation "input_files/test.gtf" --output_csv "output_files/output_csv.txt" --transcript_number 50  --output_gtf "output_files/output_gtf.gtf" --input_csv "input_files/expression.csv"


